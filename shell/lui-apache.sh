#!/bin/sh

scriptdir="$(dirname $0)"
# -c must be the 1st argument
if [ "$1" = "-c" ]
then
    COMP="$2"
    shift 2
fi
# -port must be the 2nd argument (added argument)
if [ "$1" = "-port" ]
then
    export PORT="$2"
    shift 2
fi
# -ajp_port must be the 2nd argument (added argument)
if [ "$1" = "-ajp_port" ]
then
    export AJP_PORT="$2"
    shift 2
fi

export OBE_ROOT="$OBEAPP_ROOT"

export APACHE_PORT=10080;
if [ "$PHASE" = "USR" ]
then
    echo 'Phase is USR'
    export OBE_ROOT="$OBEAPP_ROOT"

    if [ -n "$FORCE_APACHE_PORT" ];
        then
        export APACHE_PORT=$FORCE_APACHE_PORT;
    else export APACHE_PORT=10200;
    fi

    echo "Apache will be served on port $APACHE_PORT"
    export APACHE_REDIRECT_HOST="172.16.99.32"
    export APACHE_REDIRECT_PORT="30090"
    export APACHE_REDIRECT_SAP="LOY"
    export CLP_REDIRECT_URI="https://dev.accounts.amadeus.com/LoginService/authorizeAngular"
    export CLP_LOGOUT_REDIRECT_URI="https://dev.accounts.amadeus.com/LoginService/logout"
    export RULES_REDIRECT_HOST="172.17.246.30"
    export RULES_REDIRECT_PORT="21003"
    export RULES_REDIRECT_SAP="1ASIUABRINTD"
fi

if [ "$PHASE" = "DEV" ]
then
    export APACHE_REDIRECT_HOST="172.17.237.47"
    export APACHE_REDIRECT_PORT="21254"
    export APACHE_REDIRECT_SAP="1ASIULOYUID"
    export CLP_REDIRECT_URI="https://dev.accounts.amadeus.com/LoginService/authorizeAngular"
    export CLP_LOGOUT_REDIRECT_URI="https://dev.accounts.amadeus.com/LoginService/logout"
    export RULES_REDIRECT_HOST="172.17.237.47"
    export RULES_REDIRECT_PORT="21254"
    export RULES_REDIRECT_SAP="1ASIULOYUID"
fi

if [ "$PHASE" = "PDT" ]
then
    export APACHE_REDIRECT_HOST="193.23.185.67"
    export APACHE_REDIRECT_PORT="21254"
    export APACHE_REDIRECT_SAP="1ASIULOYUI"
    export CLP_REDIRECT_URI="https://pdt.accounts.amadeus.com/LoginService/authorizeAngular"
    export CLP_LOGOUT_REDIRECT_URI="https://pdt.accounts.amadeus.com/LoginService/logout"
    export RULES_REDIRECT_HOST="193.23.185.67"
    export RULES_REDIRECT_PORT="21254"
    export RULES_REDIRECT_SAP="1ASIULOYUI"
fi

if [ "$PHASE" = "SKL" ]
then
    export APACHE_REDIRECT_HOST="193.23.185.67"
    export APACHE_REDIRECT_PORT="21254"
    export APACHE_REDIRECT_SAP="1ASIULOYUIK"
    export CLP_REDIRECT_URI="https://skl.accounts.amadeus.com/LoginService/authorizeAngular"
    export CLP_LOGOUT_REDIRECT_URI="https://skl.accounts.amadeus.com/LoginService/logout"
    export RULES_REDIRECT_HOST="193.23.185.67"
    export RULES_REDIRECT_PORT="21254"
    export RULES_REDIRECT_SAP="1ASIULOYUIK"
fi

if [ "$PHASE" = "MIG" ]
then
    export APACHE_REDIRECT_HOST="193.23.185.67"
    export APACHE_REDIRECT_PORT="21254"
    export APACHE_REDIRECT_SAP="1ASIULOYUIM"
    export CLP_REDIRECT_URI="https://mig.accounts.amadeus.com/LoginService/authorizeAngular"
    export CLP_LOGOUT_REDIRECT_URI="https://mig.accounts.amadeus.com/LoginService/logout"
    export RULES_REDIRECT_HOST="193.23.185.67"
    export RULES_REDIRECT_PORT="21254"
    export RULES_REDIRECT_SAP="1ASIULOYUIM"
fi

if [ "$PHASE" = "UAT" ]
then
    export APACHE_REDIRECT_HOST="193.23.185.67"
    export APACHE_REDIRECT_PORT="21254"
    export APACHE_REDIRECT_SAP="1ASIULOYUIU"
    export CLP_REDIRECT_URI="https://uat.accounts.amadeus.com/LoginService/authorizeAngular"
    export CLP_LOGOUT_REDIRECT_URI="https://uat.accounts.amadeus.com/LoginService/logout"
    export RULES_REDIRECT_HOST="193.23.185.67"
    export RULES_REDIRECT_PORT="21254"
    export RULES_REDIRECT_SAP="1ASIULOYUIU"
fi

if [ "$PHASE" = "FVT" ]
then
    export APACHE_REDIRECT_HOST="193.23.185.67"
    export APACHE_REDIRECT_PORT="21254"
    export APACHE_REDIRECT_SAP="1ASIULOYUIF"
    export CLP_REDIRECT_URI="https://fvt.accounts.amadeus.com/LoginService/authorizeAngular"
    export CLP_LOGOUT_REDIRECT_URI="https://fvt.accounts.amadeus.com/LoginService/logout"
    export RULES_REDIRECT_HOST="193.23.185.67"
    export RULES_REDIRECT_PORT="21254"
    export RULES_REDIRECT_SAP="1ASIULOYUIF"
fi

if [ "$PHASE" = "BTPRD" ]
then
    export APACHE_REDIRECT_HOST="193.23.185.67"
    export APACHE_REDIRECT_PORT="21254"
    export APACHE_REDIRECT_SAP="1ASIULOYUIR"
    export CLP_REDIRECT_URI="https://btprd.accounts.amadeus.com/LoginService/authorizeAngular"
    export CLP_LOGOUT_REDIRECT_URI="https://btprd.accounts.amadeus.com/LoginService/logout"
    export RULES_REDIRECT_HOST="193.23.185.67"
    export RULES_REDIRECT_PORT="21254"
    export RULES_REDIRECT_SAP="1ASIULOYUIR"
fi

if [ "$PHASE" = "PPT" ]
then
    export APACHE_REDIRECT_HOST="172.17.238.21"
    export APACHE_REDIRECT_PORT="21254"
    export APACHE_REDIRECT_SAP="1ASIULOYUI"
    export CLP_REDIRECT_URI="https://www.accounts.amadeus.com/LoginService/authorizeAngular"
    export CLP_LOGOUT_REDIRECT_URI="https://www.accounts.amadeus.com/LoginService/logout"
    export RULES_REDIRECT_HOST="172.17.238.21"
    export RULES_REDIRECT_PORT="21254"
    export RULES_REDIRECT_SAP="1ASIULOYUI"
fi

if [ "$PHASE" = "PRD" ]
then
    export APACHE_REDIRECT_HOST="194.76.166.67"
    export APACHE_REDIRECT_PORT="21254"
    export APACHE_REDIRECT_SAP="1ASIULOYUI"
    export CLP_REDIRECT_URI="https://www.accounts.amadeus.com/LoginService/authorizeAngular"
    export CLP_LOGOUT_REDIRECT_URI="https://www.accounts.amadeus.com/LoginService/logout"
    export RULES_REDIRECT_HOST="194.76.166.67"
    export RULES_REDIRECT_PORT="21254"
    export RULES_REDIRECT_SAP="1ASIULOYUI"
fi

${scriptdir}/../pack/AWS/bin/du-apache.sh -c $COMP -p $PHASE $@
exit $?

# ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1'
