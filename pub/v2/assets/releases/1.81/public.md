## v1.81

#### Front office

- Company approving the complimentary tier can now be seen in the associated history
- Individual member's title can now be viewed and edited 

#### Fixes

---

- Creating an outgoing retroclaim that ends up in status NEW is no longer seen as an error (PTR 17167248)
- Cards with a fulfilment status in 'Error' can now be reiussed (PTR17466243)
- Long member names in tier synchronization panel no longer breaks the display (PTR16847656)
- Clear search in catalog administration now works correctly (PTR16862399)
- When refunding, switching from 'Full' to 'Forced' no longer leads to having the 'next' button unclickable (PTR16808782)