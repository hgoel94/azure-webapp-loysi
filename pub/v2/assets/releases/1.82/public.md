## v1.82

#### Fixes

---

- Refund button is now correctly shown in case of exchange
- Manual adjustment expiry date can no longer be in the past