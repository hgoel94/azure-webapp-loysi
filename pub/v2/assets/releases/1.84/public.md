## v1.84

#### Front office

- Nationalities of a member can now be managed
- Accrual now has offers to navigate to its associated parent request when it was created through a retroclaim
- Simple search now includes search by document number. Document number can be shown in the result list by adding it to the list of displayed columns
