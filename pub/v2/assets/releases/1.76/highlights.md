- Accrual / retroclaims pending queues have been improved to help your work!

    **You can now see the difference between member profile and request in case of name mismatch and correct it directly  
    First name or last name on the request is highlighted in orange in case it is not strictly equal to the corresponding one in the profile (it is not case sensitive)**  

- You can now be notified of new features
  If you do not want to be alerted anymore, you can change that in the preference panel