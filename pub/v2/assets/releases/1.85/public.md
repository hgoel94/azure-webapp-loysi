## v1.85

#### Front office

- Enrolment form has been changed to have gender appearing before name as the title depends on it

#### Fixes
- Tag server configuration now prevails (i.e. tags defined as badges though they have an icon now display correctly as badges)
- Migrated memberships with 12-chars long IDs now correctly appear on pending queues filtered on the 'name mismatch' error