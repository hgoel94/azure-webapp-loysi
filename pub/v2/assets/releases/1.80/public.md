## v1.80

#### General information

- A small look'n'feel revamping is on going: it will not affect the way of using the application but should lead to cleaner and clearer display

#### Fixes

---

- Payments now appear correctly when data is provided in the transaction details
- Local filtering of data tables now works properly when user starts to filter while not being on page 1
