## v1.85

#### Front office

- Enrolment screens new fields (VEL, ENR)
- Preferred name editable and can be viewed (VEL)
- Security questions can now be managed (VEL)
- Accrual form evolution to support revenue based (VEL, ENR)
- Personal information enriched with promotion code, referrall, employment details, letter of salutation (VFF, ENR)

### 1A only
- Proof of concept of customer profitability can be enabled via the Experimental features preference