## v1.77

#### Features

---

##### Administration

- Administrators can now easily duplicate data from the redemption catalog items by using the "Clone" functionality

#### Fixes

---

- A display issue on the membership dashboard overview has been fixed, long description on retroclaims was leading to a minor display issue
- PTR 17153758 - Success message no longer appear several times after multiple enrolments
