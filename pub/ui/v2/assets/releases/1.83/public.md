## v1.83

#### Fixes

- PTR 16773521 - Origin and destination in accrual request are now mandatory or not according to the configuration
- PTR 16846848 - Removing frequent flyer id in booking info as it is never filled

---
