## v1.79

#### Features

---

##### Front office

- Dates pickers have been improved: you no longer need to type the separators (i.e.: type 30032019 and the date picker be filled as 30/03/2019)
- The default search option selected is now 'Membership id'
- The list of last visited profiles has been extended to 10 elements
- You can now search member by middle name in the advanced search. To activate it in the quick search as well, enable it in the General preferences.

##### Back office

- Several name mismatch can now be rejected or reprocessed by correcting the name

  _You can now see the difference between member profile and request in case of name mismatch and correct it directly  
   in the list of pending accruals. To do this, filter by the associated error (37620 - The name is not matching the member identification information)._

#### Fixes

---

- The list of last visited profiles is now correctly reordered
