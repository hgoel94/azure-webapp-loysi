- Several name mismatch can now be rejected or reprocessed by correcting the name

  **You can now see the difference between member profile and request in case of name mismatch and correct it directly  
   in the list of pending accruals. To do this, filter by the associated error (37620 - The name is not matching the member identification information). **

- You can now search member by middle name in the advanced search. To activate it in the quick search as well, enable it in the General preferences.