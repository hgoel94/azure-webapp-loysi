## v1.78

#### Features

---

- CR17209345 - When entering an air accrual, fare basis is capitalized by default
- On accrual and retroclaims, user can now see meta information and know who created / modified the request
- Progressive Web App - Now the Agent UI is accessible offline, featuring load time improvement and new version preinstall and notification.

#### Fixes

---

- Logout is now properly invalidating the authentication token, thus, users having monosign enabled should not experience issues if they disconnect and reconnect
- Enrolment screen is now linked to the associated LSS permissions
- IR17232808 - More explicit error message when connection is refused