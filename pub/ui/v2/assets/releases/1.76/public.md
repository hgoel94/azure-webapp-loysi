## v1.76

#### Features

---

##### Front office

- Membership search has been improved

  - The 20 results limitation has been extended to 100. You can scroll down the page to get more results
  - Members can now be searched by fraud status
  - Merged members are now clearly identified in the search result list and the merge status is highlighted on the member dashboard via an icon next to their avatar

![Illustration of the merged member](assets/releases/1.76/static/Merged_member.png)

- When editing a membership, based on a configuration set by your administrator, tags can now be displayed as badges rather than icons
- Manual adjustments now use the expiry timme from the system configuration on top of the date. It is shown when you force the expiry date
- Nominees and sponsors now have filters

##### Back office

- Accrual / retroclaims pending queues have been improved

  **You can now see the difference between member profile and request in case of name mismatch and correct it directly  
   First name or last name on the request is highlighted in orange in case it is not strictly equal to the corresponding one in the profile (it is not case sensitive)**

![Illustration of the name mismatch tool](assets/releases/1.76/static/Name_mismatch.png)

#### Miscellaneous

---

- Tier name now appear fully when selecting one of them in the tier history in the associated details display
- Error messages displayed now contain error code  
  Also, in case there is only one error, the prefix 1) which made the user think there would be several errors, no longer appears
- Number display have been aligned on all screens (i.e. 10000 is now displayed as 10 000 to ease the reading)
- Adding several missing permissions

#### Fixes

---

- PTR 17064810 [Medium]- FI:LOY:PROD:Partner Reservation Number value in Crane
- PTR 17056709 [Medium]: 1A:FI:LOY:UAT:UAT:UI:Missing PO box field on UI
- PTR 17050495 [Medium]: 1A:FI:LOY:PDT:FVT:UI:Unable to purchase non-chargeable item for user not allowed to override price
  _From now on, on redemption screen:  
  If pricing is owned by the program, user can override it only  
  If he’ s granted the permission CREATE_FFP_PURCHASE_RQ_OVERRIDE_CATALOGUE_PRICE  
  If not, any user can override it, even without the permission_
- Billing values are now correctly refreshed upon accrual update
- Column names are now shown on the list of recently visited profiles
- "No existing address" was shown when member had no phone
- Safe check added on organisation memberships to avoid having a browser crash in case of missing identity information
- Permissions have been added on nominees to avoid getting success messages due to partial updates permissions check
